# Example Use of the Image

This directory contains a few test scenarios for the image.

## Standalone

* copy .env.sample to .env
* run `docker-compose up`

## composer module

* copy these files into your composer module folder:
  * .env.sample
  * .docker/ contents
  * docker-compose.yml
  * append .gitignore.sample to your .gitignore
* copy .env.sample to .env
* add a new folder 'vendor' (the image will install Magento modules in here so that you can debug locally)
* if your module 'require's other modules, add your COMPOSER\_AUTH to .env
* if you want to customise the available Magento modules, edit .docker/etc/config.php
* run `docker-compose run --rm --service-ports magento shell`

In the container, you can use 'magerun2', 'composer' and other tools.

You can connect to the DB from localhost on port 3306

## Logs

If you want to see log files, either look in /var/log/nginx in the container, or map a local folder to /var/log/nginx in your docker-compose.yml.


