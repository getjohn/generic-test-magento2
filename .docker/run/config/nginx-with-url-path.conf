server {
    listen 80;

    server_name _;
		set $MAGE_ROOT /opt/getjohn/container/magento;
		set $FPM_HOST 127.0.0.1:9000;
        root $MAGE_ROOT/pub;

        location /.well-known {
		}

		index index.php;
		autoindex off;
		charset UTF-8;
		error_page 404 403 = /errors/404.php;
		#add_header "X-UA-Compatible" "IE=Edge";

		location {URL_PATH}/ {
			try_files $uri $uri/ {URL_PATH}/index.php$is_args$args;
		}

		location {URL_PATH}/static/ {
			# Uncomment the following line in production mode
			# expires max;

			# Remove signature of the static files that is used to overcome the browser cache
			location ~ ^{URL_PATH}/static/version {
				rewrite ^{URL_PATH}/static/(version\d*/)?(.*)$ {URL_PATH}/static/$2 last;
			}

			location ~* \.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$ {
				add_header Cache-Control "public";
				add_header X-Frame-Options "SAMEORIGIN";
				expires +1y;

				if (!-f $request_filename) {
					rewrite ^{URL_PATH}/static/?(.*)$ {URL_PATH}/static.php?resource=$1 last;
				}
			}
			location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
				add_header Cache-Control "no-store";
				add_header X-Frame-Options "SAMEORIGIN";
				expires    off;

				if (!-f $request_filename) {
					rewrite ^{URL_PATH}/static/?(.*)$ {URL_PATH}/static.php?resource=$1 last;
				}
			}
			if (!-f $request_filename) {
				rewrite ^{URL_PATH}/static/?(.*)$ {URL_PATH}/static.php?resource=$1 last;
			}
			add_header X-Frame-Options "SAMEORIGIN";
		}

		location {URL_PATH}/media/ {
			try_files $uri $uri/ {URL_PATH}/get.php$is_args$args;

			location ~ ^{URL_PATH}/media/theme_customization/.*\.xml {
				deny all;
			}

			location ~* \.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$ {
				add_header Cache-Control "public";
				add_header X-Frame-Options "SAMEORIGIN";
				expires +1y;
				try_files $uri $uri/ /get.php$is_args$args;
			}
			location ~* \.(zip|gz|gzip|bz2|csv|xml)$ {
				add_header Cache-Control "no-store";
				add_header X-Frame-Options "SAMEORIGIN";
				expires    off;
				try_files $uri $uri/ /get.php$is_args$args;
			}
			add_header X-Frame-Options "SAMEORIGIN";
		}

		location {URL_PATH}/media/customer/ {
			deny all;
		}

		location {URL_PATH}/media/downloadable/ {
			deny all;
		}

		location {URL_PATH}/media/import/ {
			deny all;
		}

		# PHP entry point for main application
		location ~ (index|get|static|report|404|503)\.php$ {
			try_files $uri =404;
			fastcgi_pass   $FPM_HOST;
			fastcgi_buffers 16 256k;
			fastcgi_buffer_size 256k;

			fastcgi_param  PHP_FLAG  "session.auto_start=off \n suhosin.session.cryptua=off";
			fastcgi_param  PHP_VALUE "memory_limit=768M \n max_execution_time=18000";
			fastcgi_read_timeout 600s;
			fastcgi_connect_timeout 600s;

			fastcgi_index  index.php;
			fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
			include        fastcgi_params;
		}

		gzip on;
		gzip_disable "msie6";

		gzip_comp_level 6;
		gzip_min_length 1100;
		gzip_buffers 16 8k;
		gzip_proxied any;
		gzip_types
			text/plain
			text/css
			text/js
			text/xml
			text/javascript
			application/javascript
			application/x-javascript
			application/json
			application/xml
			application/xml+rss
			image/svg+xml;
		gzip_vary on;
    # Banned locations (only reached if the earlier PHP entry point regexes don't match)
    location ~* (\.php$|\.htaccess$|\.git) {
        deny all;
    }

		access_log /var/log/nginx/access.log combined;
		error_log /var/log/nginx/error.log warn;
}
