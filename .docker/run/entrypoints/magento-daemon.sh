#!/bin/bash

# This entrypoint simply runs Magento, with php-fpm in the foreground

set -e 

cd /opt/getjohn/container/magento

echo ========================================================
echo Magento is running on $GJ_WEBSITE_URL
echo Waiting for termination...
echo ========================================================

trap 'kill $SLEEP' SIGTERM
sleep infinity &
SLEEP=$!
wait $SLEEP
RESULT=$?
exit $RESULT

