#!/bin/bash
#
# main entrypoint, processes the command line arguments,
# defaults to the CMD setting in Dockerfile which is "daemon"

set -e

cd /opt/getjohn/container/magento
: ${GJ_WEBSITE_CODE:=base}
: ${GJ_ELASTIC_HOST:=127.0.0.1}
: ${MYSQL_HOST:=mysql}
: ${GJ_SERVICES_WAIT:=240}
: ${GJ_DB_STRIP:=@stripped}
: ${GJ_SKIP_NPM:=1}

# link .docker scripts into the magento root
if [ ! -L .docker ]; then
	rm -rf .docker # just in case we upgraded from an older image where we copied the files instead of linking
	ln -s /opt/getjohn/container/.docker .
fi

# if .docker/composer/ exists, 
if [ -d composer ]; then
	if [ ! -r composer/composer.json ]; then
		echo "===================================\nNo composer.json found in Magento root - copying our default version and installing all modules....\n======\n"
		cp -Rp /opt/getjohn/container/.docker/composer.* composer/
		IS_FRESH_INSTALL=1
	fi
	rm -rf composer.json composer.lock
	ln -s composer/composer.json .
	ln -s composer/composer.lock .
fi
if [ ! -r composer.json -a ! -d composer ]; then
	echo "===================================\nNo composer.json found in Magento root - copying our default version and installing all modules....\n======\n"
	# force our composer files to be used, even if it's an existing setup - in development you can set GJ_SKIP_COMPOSER to avoid this
	cp -Rp /opt/getjohn/container/.docker/composer.* .
	IS_FRESH_INSTALL=1
fi

set -e
if [ -n "$PRE_INIT_SCRIPT" ]; then
	echo "Running pre-setup-script $PRE_INIT_SCRIPT"
	$GIT_CLONE_DIR/$PRE_INIT_SCRIPT
fi

# load compose module if provided:
if [ -n "$GIT_CLONE_DIR" ] && [ -r "$GIT_CLONE_DIR/composer.json" ]; then
	.docker/run/scripts/module-load.sh
fi

if [ -z "$GJ_SKIP_COMPOSER" ]; then
	if [ ! -r app/etc/vendor_path.php ]; then
		rm -rf vendor/magento vendor/composer; # force re-install because critical include doesnt exist
	fi
	composer install --no-interaction
	if [ ${IS_FRESH_INSTALL:=0} -eq 1 -a ! -r vendor/magento/framework/.ISSUE-33803.patch.applied ]; then
		git apply -v .docker/run/scripts/ISSUE-33803.patch
		touch vendor/magento/framework/.ISSUE-33803.patch.applied
	fi
fi

if [ 0 -eq "$GJ_SKIP_NPM" ]; then
	if [ ! -r package.json ]; then
		cp package.json.sample package.json
		cp Gruntfile.js.sample Gruntfile.js
		cp ../.docker/run/config/grunt-config-less.js dev/tools/grunt/configs/less.js
	fi
	if [ ! -d node_modules ]; then
		npm install
	fi
fi

# if no explicit URL set, and we have a ngrok token, create a tunnel and set the URL from that:
if [ -z "$GJ_WEBSITE_URL" ] && [ -n "$NGROK_TOKEN" ]; then
	. .docker/run/scripts/start-ngrok-tunnel.sh
	GJ_WEBSITE_URL=$NGROK_URL
fi

# add trailing slash if not given:
GJ_WEBSITE_URL=`echo $GJ_WEBSITE_URL|sed -E 's#/?$#/#'`
# get the path part if there is one:
URL_PATH=`echo $GJ_WEBSITE_URL | sed -E 's#.*//[^/]+(.*)/$#\1#'`

: ${GJ_WEBSITE_URL:?Please set GJ_WEBSITE_URL for local access, or provide NGROK_TOKEN for public access}

# apply the path if there is one:
if [ -n "$URL_PATH" ]; then
	echo "Updating nginx config with URL Path $URL_PATH..."
	cat .docker/run/config/nginx-with-url-path.conf |sed "s#{URL_PATH}#$URL_PATH#g" >/etc/nginx/sites-enabled/default
	# ugly hack to make Magento in a URL path work - should be possible to handle this in nginx conf:
	if [ ! -r "pub$URL_PATH" ]; then
		mkdir -p pub$URL_PATH
		pushd pub$URL_PATH
		for i in errors get.php health_check.php index.php media opt static static.php; do
			ln -s /opt/getjohn/container/magento/pub/$i .
		done
		popd
	fi
fi

echo "Waiting for services to start..."
.docker/run/scripts/wait-for-it.sh $MYSQL_HOST:3306 -t $GJ_SERVICES_WAIT
.docker/run/scripts/wait-for-it.sh $GJ_ELASTIC_HOST:9200 -t $GJ_SERVICES_WAIT # takes forever... give it 768m and it speeds up a little

# force a reinstall if no app/etc/env:
if [ ${GJ_DB_RESET:=0} -eq 0 -a -z "$GJ_DB_IMPORT" -a ! -r app/etc/env.php ]; then
	echo "
	=======================================================================================================
	You have no app/etc/env.php file - re-installing Magento unless you hit CTRL+C in the next 20 seconds...
	======================================================================================================="
	sleep 20
	GJ_DB_RESET=1
fi

# import:
if [ -n "$GJ_DB_IMPORT" ]; then
	# clear cache
	rm -rf generated/code var/cache/*

	# add default env file and config if needed:
	if [[ ! -r app/etc/env.php ]] || [[ -z `grep install app/etc/env.php` ]] ; then
		cp ../app-etc-defaults/* app/etc/
	fi

	# drop and re-import DB:
	mysql -h mysql -u dbuser -pdbpass dbname -B --skip-column-names -e 'drop database dbname; create database dbname;'
	GJ_DB_IMPORT_OPTIONS=
	if [[ $GJ_DB_IMPORT == *".gz" ]]; then
		GJ_DB_IMPORT_OPTIONS="--compression gzip"
	fi
	magerun2 db:import --skip-root-check $GJ_DB_IMPORT_OPTIONS /tmp/sql/$GJ_DB_IMPORT
	# ensure upgrade runs if we did a DB import:
	GJ_SKIP_UPGRADE=
	GJ_DB_RESET=0
fi

# request a reinstall if no database:
if [ ${GJ_DB_RESET:=0} -eq 0 ]; then
	set +e
	echo 'select * from core_config_data limit 1' | mysql -u dbuser -pdbpass dbname  >/dev/null 2>&1
	RESULT=$?
	set -e
	if [ $RESULT -ne 0 ]; then
		echo "
		=======================================================================================================
		Magento database not detected - please either set GJ_DB_RESET=1 or GJ_DB_IMPORT to set up your database
		======================================================================================================="
		exit 1
	fi
fi


# install Magento
if [ ${GJ_DB_RESET:=0} -eq 1 ]; then
	rm -rf generated/code var/cache/* var/views_preprocessed pub/static/*
	rm -f app/etc/env.php
	# drop and re-create the DB:
	mysql -h mysql -u dbuser -pdbpass dbname -B --skip-column-names -e 'drop database dbname; create database dbname;'
	echo "
	=================================================
	Installing Magento after dropping the database...
	================================================="
	bin/magento setup:inst --cleanup-database \
		--admin-firstname gjadmin --admin-lastname gjadmin --admin-email gjadmin@example.com \
		--admin-user gjadmin --admin-password gjadmin321 \
		--base-url http://localhost \
		--backend-frontname admin \
		--db-host mysql --db-password dbpass --db-user dbuser --db-name dbname \
		--use-rewrites 1 --currency EUR --language en_GB \
		--search-engine elasticsearch7 --elasticsearch-host $GJ_ELASTIC_HOST --elasticsearch-port 9200
	magerun2 config:set --skip-root-check admin/url/use_custom_path 0
	GJ_SKIP_UPGRADE=
	GJ_SKIP_COMPILE=

fi

magerun2 config:store:set --skip-root-check --scope=default dev/template/allow_symlink 1
magerun2 config:store:set --skip-root-check --scope=default catalog/search/elasticsearch7_server_hostname $GJ_ELASTIC_HOST
magerun2 config:store:set --skip-root-check --scope=default web/unsecure/base_url $GJ_WEBSITE_URL
magerun2 config:store:set --skip-root-check --scope=default web/secure/base_url $GJ_WEBSITE_URL
magerun2 config:store:set --skip-root-check --scope=websites --scope-id=$GJ_WEBSITE_CODE web/unsecure/base_url $GJ_WEBSITE_URL
magerun2 config:store:set --skip-root-check --scope=websites --scope-id=$GJ_WEBSITE_CODE web/secure/base_url $GJ_WEBSITE_URL
magerun2 admin:user:change-password --skip-root-check gjadmin gjadmin321
magerun2 setup:config:set --skip-root-check --backend-frontname="admin"

# build:
if [ -z "$GJ_SKIP_UPGRADE" ]; then
	magerun2 set:up --skip-root-check
fi
if [ -z "$GJ_SKIP_COMPILE" ]; then
	magerun2 set:di:com --skip-root-check
fi

if [ -n "$POST_INIT_SCRIPT" ]; then
	echo "Running post-setup-script $POST_INIT_SCRIPT"
	$GIT_CLONE_DIR/$POST_INIT_SCRIPT
fi

# start nginx (by default, in the background)
nginx
# php-fpm running as root, in the background
php-fpm -D -R
# start MTA to send to mailhog
service nullmailer start

FRONTNAME=`php -r '$env=require("app/etc/env.php");echo $env["backend"]["frontName"];'`

cat << EOF
========================
	Magento is running on $GJ_WEBSITE_URL
	Admin: $GJ_WEBSITE_URL$FRONTNAME
	Admin user: gjadmin
	Admin pass: gjadmin321
============================
EOF

case "$@" in
ghost) .docker/run/scripts/ghostinspector-test-run.sh;;
shell) .docker/run/entrypoints/magento-shell.sh;;
*) sleep infinity;;
esac
RESULT=$?

if [ -n "$GJ_DB_EXPORT" ]; then
	echo -e "=======================\nShutting down - first exporting DB:"
	cd /opt/getjohn/container/magento
	GJ_DB_EXPORT_OPTIONS=
	if [[ $GJ_DB_EXPORT == *".gz" ]]; then
		GJ_DB_EXPORT_OPTIONS="--compression gzip"
	fi
	magerun2 db:dump --skip-root-check $GJ_DB_EXPORT_OPTIONS --strip="$GJ_DB_STRIP" /tmp/sql/$GJ_DB_EXPORT
	# the sql file will be owned by root, so if it's in an external volume, ensure it's usable:
	chmod 777 /tmp/sql/$GJ_DB_EXPORT
fi
exit $RESULT

