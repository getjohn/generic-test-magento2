#!/bin/bash

set -e 

cd /opt/getjohn/container/magento

if [ -t 0 -a -t 1 ]; then
	/bin/bash
	exit 0
fi

echo "Magento shell requested in non-interactive mode... I'm just gonna wait right here for a while..."
read

