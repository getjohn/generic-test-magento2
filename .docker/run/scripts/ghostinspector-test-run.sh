#!/bin/bash

# This entrypoint assumes a ghostinspector/test-runner-standalone container is present.
# It discovers the public URL, applies that to Magento,,
# runs Magento then waits for the ghostinspector container to exit

set -e 
# export any variables that we set in this script:
set -o allexport

: ${GJ_WEBSITE_URL:?Could not get public URL from ngrok on $GJ_NGROK_HOST}
: ${GI_API_KEY:?GI_API_KEY is not set - find this in your GhostInpsector account}
: ${GI_SUITE_ID:?GI_SUITE_ID is not set - record your test, then find the key in your account and set it}
# default to 11 minutes timeout per GhostInspector documentation:
: ${GI_MAX_TIME:=660}

# GI_TEST_START_PATH can also be set - add it to the start URL:
GI_START_URL="$GJ_WEBSITE_URL$GI_TEST_START_PATH"

cd /opt/getjohn/container/magento

# start the test:
echo -e "=================================Running test suite:\nStart URL: $GI_START_URL"

RESULTFILE=`tempfile`
curl --silent --max-time $GI_MAX_TIME -X POST -d "apiKey=$GI_API_KEY" -d "startUrl=$GI_START_URL" https://api.ghostinspector.com/v1/suites/$GI_SUITE_ID/execute/ >$RESULTFILE
GI_SUITE_RESULT_ID=`jq .data[0].suiteResult <$RESULTFILE`
GI_SUITE_NAME=`jq .data[0].name <$RESULTFILE`
echo "Result ID: $GI_SUITE_RESULT_ID"
echo "Suite Name: $GI_SUITE_NAME"

GI_SUITE_STATUS=`jq .data[0].passing <$RESULTFILE`
GI_SUITE_TIME=`jq .data[0].executionTime <$RESULTFILE`
# if the test isn't finished, loop until it is:
while [ "$GI_SUITE_STATUS" == "null" ]; do
	echo "....elapsed time $GJ_SUITE_TIME"
	sleep 20
	rm $RESULTFILE
	curl --silent --max-time $GI_MAX_TIME -d "apiKey=$GI_API_KEY" https://api.ghostinspector.com/v1/suite-results/$GI_SUITE_RESULT_ID/ >$RESULTFILE
	# (note the result file format is slightly different now, for an individual suite result)
	GI_SUITE_STATUS=`jq .data.passing <$RESULTFILE`
	GI_SUITE_TIME=`jq .data.executionTime <$RESULTFILE`
done

echo Result: $GI_SUITE_STATUS
echo ============================
if [ "$GI_SUITE_STATUS" == "true" ]; then
	exit 0
fi
exit 1

