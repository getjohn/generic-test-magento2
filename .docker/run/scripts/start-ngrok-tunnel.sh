#!/bin/bash

# This file should be SOURCED not executed, so that it can set the NGROK_URL environment var
# eg.   . .docker/run/scripts/start-ngrok-tunnel.sh

# Intialize ngrok and open tunnel to our application

echo "Ngrok options: $NGROK_OPTIONS"
ngrok authtoken $NGROK_TOKEN
ngrok http ${NGROK_OPTIONS:=} 80 >/dev/null &
NGROK_PID=$!

# give ngrok a second to register URLs
: ${NGROK_STARTUP_DELAY:=2}
>&2 echo "Waiting $NGROK_STARTUP_DELAY seconds for ngrok to register..."
sleep $NGROK_STARTUP_DELAY

# double-check the ngrok is running before we continue
if ! kill -0 $NGROK_PID > /dev/null 2>&1; then
    >&2 echo "FATAL: Unable to signal ngrok process, is the token valid?"
    exit 1
fi

# Grab the ngrok url to send to the API, coerce to HTTPS
NGROK_URL=$(curl -s 'http://localhost:4040/api/tunnels' | jq -r '.tunnels[1].public_url' | sed 's/http:/https:/')
if [ "$NGROK_URL" = "" ]; then
  >&2 echo "ERROR: did not get a start URL from the ngrok daemon, exiting..."
  exit 2
fi

echo Tunnel set up from $NGROK_URL


