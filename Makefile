
local:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build

build:
	.docker/build/docker-build.sh

push: build
	.docker/build/docker-push.sh

