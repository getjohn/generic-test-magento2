# syntax = docker/dockerfile:latest
#
# Get John Magento test installation
#
# This dockerfile builds a container which will load up a specific
# DB snapshot of this Magento installation
#
# The default entrypoint will set the URL of the website, 
# before running nginx and php-fpm and then waiting for CTRL+C
# 
# Environment variables - see .env.sample
#
# Part based on https://github.com/markhilton/docker-php-fpm/blob/master/7.3/Dockerfile
#
FROM php:8.1-fpm

# we don't add DEBIAN_FRONTEND with ENV because it would persist into the container
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update   -qq 

# apt packages:
RUN apt-get install -y -qq wget git ssh mariadb-client unzip vim rsync nginx jq nullmailer

# extensions already in: ctype curl hash iconv pic ftp mbstring mysqlnd argon2 sodium pdo_sqlite sqlite3 libedit openssl zlib pear
# official Magento 2.4 required exts: bcmath ctype curl dom gd hash iconv intl mbstring openssl pdo_mysql simplexml soap xsl zip sockets

# for php extensions - not needed, all preinstalled except for gd:
RUN apt-get install -y -qq libjpeg62-turbo-dev libpng-dev libxml2-dev libxslt-dev libsodium-dev libzip-dev libfreetype6-dev
RUN pecl install xdebug
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install bcmath gd intl pdo_mysql simplexml soap xsl zip sockets
RUN docker-php-ext-enable bcmath gd intl pdo_mysql simplexml soap xsl zip sockets xdebug

# NPM and Grunt for theme development:
RUN apt-get install -y -qq grunt npm

# Composer:
RUN EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"; \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"; \
    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]; \
    then \
        >&2 echo 'ERROR: Invalid installer signature'; \
        rm composer-setup.php; \
        exit 1; \
    fi; \
    php composer-setup.php --quiet && mv composer.phar /usr/local/bin/composer2 && chmod +x /usr/local/bin/composer2 && \
    php composer-setup.php --quiet --1 && mv composer.phar /usr/local/bin/composer1 && chmod +x /usr/local/bin/composer1 && \
    ln -s /usr/local/bin/composer2 /usr/local/bin/composer

# magerun2:
RUN curl -sS -O https://files.magerun.net/n98-magerun2-latest.phar && \
    curl -sS -o n98-magerun2-latest.phar.sha256 https://files.magerun.net/sha256.php?file=n98-magerun2-latest.phar && \
    shasum -a 256 -c n98-magerun2-latest.phar.sha256 && \
    mv n98-magerun2-latest.phar /usr/local/bin/magerun2 && \
    chmod +x /usr/local/bin/magerun2

# Install ngrok (latest official stable from https://ngrok.com/download).
# (taken from https://github.com/ghost-inspector/docker-test-runner )
ADD https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip /ngrok.zip
RUN set -x \
 && unzip -o /ngrok.zip -d /usr/local/bin \
 && rm -f /ngrok.zip

# TOOD do we even need this?
RUN apt-get clean -qq

# forward to mailhog
COPY .docker/build/nullmailer /etc/nullmailer

# install nginx config file
COPY .docker/build/nginx/www.conf /etc/nginx/sites-enabled/default

# install php FPM config
ADD .docker/build/fpm/php.ini /usr/local/etc/php/
COPY .docker/build/fpm/pool.d/www.conf /usr/local/etc/php-fpm.d/www.conf

# install the php sendmail setting for mailhog

WORKDIR /opt/getjohn/container/magento

COPY .docker/run /opt/getjohn/container/.docker/run
RUN mkdir -p /tmp/sql
RUN mkdir -p /var/log/php-fpm
RUN mkdir -p /opt/getjohn/container/magento

# default config files:
COPY app/etc /opt/getjohn/container/app-etc-defaults

# composer files - during entrypoint.sh we copy these into the magento volume, if it's empty
COPY composer.* /opt/getjohn/container/.docker/
## workaround for https://github.com/moby/moby/issues/42195 (not needed, using $COMPOSER_AUTH now)
#COPY .docker/build/empty-auth.json auth.json

# pre-build the compiled files, so we don't have to on every run
#RUN bin/magento setup:di:compile

# Magento frontend, admin, and Mailhog:
EXPOSE 80
# ngrok API:
EXPOSE 4040
# Mailhog:
EXPOSE 1025
EXPOSE 8025

# entrypoint will be overridden by bitbucket pipeline so run it manually from the pipeline script:
ENTRYPOINT ["/opt/getjohn/container/.docker/run/entrypoints/entrypoint.sh"]

# default command - our entrypoint script checks this
# If you want to examine the container, set the command to /bin/bash or "shell"
CMD ["daemon"]

