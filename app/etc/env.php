<?php
return [
    'install' => [
        'date' => 'Some date'
    ],
    'backend' => [
        'frontName' => 'admin'
    ],
    'remote_storage' => [
        'driver' => 'file'
    ],
    'queue' => [
        'consumers_wait_for_messages' => 1
    ],
    'system' => [
        'default' => [
            'smile_elasticsuite_core_base_settings' => [
                'es_client' => [
                    'servers' => $_ENV['GJ_ELASTIC_HOST'] . ':9200',
                    'enable_https_mode' => 0,
                    'http_auth_user' => '',
                    'http_auth_pwd' => '',
                    'enable_http_auth' => false
                ]
            ],
            'catalog' => [
                'search' => [
                    'elasticsearch7_server_hostname' => 'elastic',
                    'elasticsearch7_server_port' => '9200',
                    'elasticsearch7_enable_auth' => '0',
                    'elasticsearch7_index_prefix' => 'webspar',
                    'elasticsearch5_server_port' => '9999', // force failure - GJDIB-1638
                    'elasticsearch6_server_port' => '9999', // force failure - GJDIB-163
                ],
            ],
        ]
    ],
    'crypt' => [
        'key' => $_ENV['GJ_MAGE_CRYPT_KEY'],
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
		'host' => 'mysql',
		'dbname' => 'dbname',
		'username' => 'dbuser',
		'password' => 'dbpass',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1',
                'driver_options' => [
                    1014 => false
                ]
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'default',
    'session' => [
        'save' => 'files'
    ],
    'cache' => [
        'frontend' => [
            'default' => [
                'id_prefix' => '361_'
            ],
            'page_cache' => [
                'id_prefix' => '361_'
            ]
        ],
        'allow_parallel_generation' => false
    ],
    'lock' => [
        'provider' => 'db',
        'config' => [
            'prefix' => ''
        ]
    ],
    'directories' => [
        'document_root_is_pub' => true
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'compiled_config' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 0,
        'config_webservice' => 1,
        'translate' => 1,
        'vertex' => 1
    ],
    'downloadable_domains' => [
        'localhost'
    ]
];
