# Magento Test Installation  #

This docker image is for development and testing of Magento core, themes and modules.  It's designed for three use cases:

1. developing a module or theme outside of a 'real' Magento installation
2. testing a 3rd party module or theme
3. running browser-based tests on a module or theme
4. running tests from a CI/CD pipeline

## Useful Notes

* requires docker-compose v1.27+
* examples: https://bitbucket.org/getjohn/generic-test-magento2/src/master/.docker/examples/
* The guides below are all about *using* the docker image - for building it, see [Build and Test](#build)

## HowTo - Getting Started

After these steps, you'll have a Magento environment running locally, and be able to access the database from a client like HeidiSQL:

1. copy the contents of the 'standalone' example to a local directory: https://bitbucket.org/getjohn/generic-test-magento2/src/master/.docker/examples/standalone/
2. rename .env.sample to .env
3. run docker-compose up
4. after it all loads, you should be able to browse to http://localhost/ and see the default Luma home page
5. you can log in to the admin at http://localhost/admin with username gjadmin, password gjadmin321
6. you can access the MySQL database on localhost port 3306
7. if Magento sends out email, it will be visible via Mailhog on http://localhost:8025/

## HowTo - Add it to an existing composer module so you can test your module

After these steps, you'll be able to run 'docker-compose' inside your module's root directory, and test it in a clean Magento instance.  This assumes you have a module or theme in a directory on its own.

1. confirm you have a composer.json file with at least the 'name' field set
2. copy these files to your module:
  * `docker-compose.yml`
  * `.env.sample`
  * `.docker`
  * `.gitignore.sample`
3. run `docker-compose run --rm --service-ports magento shell`

At this point, if your module 'require's other modules, you may need to add the contents of your auth.json - do this by editing the COMPOSER\_AUTH variable in your .env file, set it to the whole contents of your auth.json file.

4. you'll land at a bash prompt inside the container, with Magento running
5. after it all loads, you should be able to browse to http://localhost/ and see the default Luma home page
6. you can log in to the admin at http://localhost/admin with username gjadmin, password gjadmin321


## HowTo - prevent overwriting the DB contents each time

1. in .env, set GJ\_DB\_IMPORT to be blank - that stops it importing new data with each run
2. you can also set GJ\_DB\_EXPORT to be a filename, eg. 'export.sql', and when you exit from the bash prompt, it will export the contents to .docker/sql/

## Features

### Database access

* access MariaDB from your client using host 'localhost' port 3306

### Outgoing Email

* access Mailhog to see any email Magento sends on http://localhost:8025/

### Nginx and FPM logs

* inside the container, you'll see logs in /var/log/nginx, /var/log/php-fpm
* Magento logs are in the usual place - var/log/ (relative path to Magento root)

### Put Magento on a Path eg. http://localhost/some-path/

* just set GJ\_WEBSITE\_URL in your .env to include the path - eg. `GJ_WEBSITE_URL=http://localhost/somepath`

### Run commands before initialising Magento

If you need to add extra composer repositories, or run other commands, add a 'pre init' script:

1. add a shell script in your repo - we suggest using `.docker/pre-init.sh`
2. in docker-compose.yml, under the 'magento' environment section, set `PRE_INIT_SCRIPT` to `.docker/pre-init`

Example:

```
#!/bin/bash
# add our private repo, so that we can include modules from it
composer config repositories.my-private-repo https://my-repo.my-company.com composer
composer require some-company/some-module
```

### Run commands after initialising Magento

Follow the 'before initialising Magento' steps above, but replace 'pre' with 'post'.

### Change which modules are enabled or disabled

For example, we disable the 2FA module by default but you might want to use it.

* edit `.docker/etc/config.php` to enable/disable as needed
* check that in your docker-compose.yml, this file is mapped to app/etc/config.php - it should be if you used the examples

### Expose your test instance on a public URL - eg. for user acceptance testing

* set up an account at https://ngrok.io/
* log into that account, and choose the 'Your AuthToken' page
* set the NGROK\_TOKEN in your .env file to that value
* run the container with `docker-compose run --rm --service-ports magento shell`
* it will give you the public URL when it has finished the setup process
* if you've set up a static subdomain in your ngrok account, you can also set NGROK\_OPTIONS eg. NGROK\_OPTIONS="--region=eu --hostname=mytest.eu.ngrok.io"

### Skip parts of the setup for faster loading

* skip the composer install - set `GJ_SKIP_COMPOSER` to any value
* skip the magento 'setup:update' command - set `GJ_SKIP_UPGRADE` to any value
  * use this if you haven't changed any class names, dependency injections, and don't need to run your module's setup functions
* skip the magento 'setup:di:compile' command - set `GJ_SKIP_COMPILE` to any value
  * use this if you haven't changed any dependency injections, observers, and have also set `GJ_SKIP_UPGRADE`
* skip the NPM install - set `GJ_SKIP_NPM=1`  (*this is the default behaviour*)

### NPM install

* set `GJ_SKIP_NPM=0` to run npm install on the default package.lock file - this gives you use of grunt for theme building and CSS source maps

## Common Errors

* 'missing entity type' or similar
  * you probably have no database - ensure it's initialised by setting GJ\_DB\_IMPORT
* 'not a directory'
  * you have some error in the volume mappings in docker-compose - perhaps mapping something which doesn't exist locally?


## Testing

This image has scripts to help automatically run browser-based tests using ghostinspector.com.

### Record a test in Ghost Inspector

* TODO: write this section

### Run a test in Ghost Inspector

3. set GI\_API\_KEY to the key shown on your Ghost Inspector profile page
4. set GI\_SUITE\_ID to the test suite ID shown on the Settings page inside the test suite
5. set NGROK\_TOKEN to the API token shown in your ngrok.io account
6. run `docker-compose run --rm --service-ports magento ghost`

The output will show whether or not the test was successful, and the result ID.

### Run a test via Bitbucket Pipelines

TODO

## Modifying and Building this Image
<a id="build"></a>

### Build and test it locally

1. run 'make local' to build the local docker-compose image
2. edit .env (copy from .docker/examples/standalone/.env if necessary) appropriately - the default should be OK
3. put your composer credentials including your Magento marketplace ones in COMPOSER\_AUTH in the .env file
4. to test it, run:

```
./run shell
```

That should run your local image, do a composer install, import an empty DB, and leave you at a command prompt with the URL set to whatever your .env file said.

### Build a new version of the image for dockerhub:

1. ensure it's tagged (run `git tag` to check) with the current Magento version
2. run 'make build'

### Push it to Dockerhub

1. ensure it builds (see above)
2. ensure your dockerhub credentials are set in `.env` (see `.env.build` for example)
3. run `make push`

## Repository Structure

* everything except `.docker/` is the contents of a regular Magento system
* `.docker/examples/` is examples of directories where you can run `docker-compose run` or `docker-compose up` using this image
* `.docker/build/` is scripts and configs used during the image build, not present in the final container
* `.docker/run/` is scripts and configs copied into the container, including the entrypoint and utility scripts


